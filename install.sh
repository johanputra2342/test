apt -y update
apt install -y sudo unzip screen wget build-essential gcc nano python3 python3-pip
sudo apt install -y xvfb
pip install --upgrade pip
pip install setuptools-rust
pip install -r requirements.txt
pip install -U requests
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt-get install -y ./google-chrome-stable_current_amd64.deb
sudo apt-get install -y -f
rm google-chrome-stable_current_amd64.deb
echo "Chrome: " && google-chrome --version
