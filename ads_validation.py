import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException, ElementNotVisibleException, ElementNotSelectableException

def check_exists_by_xpath(browser,xpath):
    try:
        browser.find_element(By.XPATH,xpath)
        browser.implicitly_wait(10)
    except NoSuchElementException:
        return False
    return True

def retry_find_element(browser,xpath,number,worker):
    count = 0

    while True:
        count +=1
        print(f"{number}. {worker} : Trying {count}!! ")

        try:
            # Wait for up to 10 seconds for the element with the ID "my-element" to be located
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
        except TimeoutException:
            # If the element is not found, try again
            continue
        else:
            # If the element is found, break out of the loop
            return True
    # count = 0
    # while True:
    #     check_xpath = check_exists_by_xpath(driver,xpath)
    #     if count < 20000:
    #         count +=1
    #         if check_xpath == True:
    #             return True

def click(driver,xpath):
    driver.find_element(By.XPATH,xpath).click()

def filling(driver,xpath,value):
    driver.find_element(By.XPATH,xpath).send_keys(value)

def get_text(driver,xpath):
    return driver.find_element(By.XPATH,xpath).text

def ads_validation(engines):
    if check_exists_by_xpath(engines,"/html/body/main/div/article/div[2]/div/div[1]/div/div[9]/div[1]/iframe[1]"):
        print("ADS FOUNDED!!")
        
        frame_video = engines.find_element(By.XPATH,"/html/body/main/div/article/div[2]/div/div[1]/div/div[9]/div[1]/iframe[1]")
        engines.save_screenshot('ss/image.png')
        engines.switch_to.frame(frame_video)
        #check jika ada tombol skip ads
        if check_exists_by_xpath(engines,"/html/body/div[1]/div[3]/div/div[2]/button"):
            print("ADA TOMBOL SKIP!")
            while True:
                if "Skip Ad" in get_text(engines,"/html/body/div[1]/div[3]/div/div[2]/button"):
                    print("bisa diklik TOMBOL SKIP!")
                    click(engines,"/html/body/div[1]/div[3]/div/div[2]/button")
                    print("sudah diklik TOMBOL SKIP!")
                    break
                
                else:
                    print("belum bisa diklik TOMBOL SKIP!")
                    time.sleep(1)
                    pass
        else:
            print("otomatis diskip!")
            #click(engines,"/html/body/div[1]/div[3]/div/div[2]/button")
            #print("sudah diklik TOMBOL SKIP!")
        time.sleep(5)   
        if check_exists_by_xpath(engines,"/html/head/script[6]"):
            print("duration ketemu")
            print(get_text(engines,"/html/head/script[6]"))
        # time.sleep(500000)
    #     # if check_exists_by_xpath(engines,"/html/body/div/iframe"):
    #     #     time.sleep(1)
    #     #     frame_video2 = engines.find_element(By.XPATH,"/html/body/div/iframe")
    #     #     engines.switch_to.frame(frame_video2)
    #     #     engines.save_screenshot('ss/image.png')
    #     #     time.sleep(1)
    #     #     if check_exists_by_xpath(engines,"/html/body/div/div/a"):
    #     #         print(get_text(engines,"/html/body/div/div/a"))

    #     #         # print(get_text(engines,"/html/body/div/div/div"))
    #     #     if check_exists_by_xpath(engines,"/html/body/div/div/div"):
    #     #         while True:
    #     #             engines.save_screenshot('ss/image.png')
    #     #             if "You can skip this ad" in get_text(engines,"/html/body/div/div/div"):
    #     #                 print(get_text(engines,"/html/body/div/div/div"))
    #     #                 time.sleep(1)
    #     #                 break
    #     #             else:
    #     #                 break
    #     #         print("Closing AD")
    #     #         engines.save_screenshot('ss/image.png')
    #     #         time.sleep(1)
    #     #         click(engines,"/html/body/div/div/div")
    #     #         engines.save_screenshot('ss/image.png')
    #     #         print("clicked and watching")
    #     #         # time.sleep(10)
    #     if check_exists_by_xpath(engines,"/html/body/div[1]/div[3]/div/div[1]/div/div"):
    #         print("ads USA")
    #         while True:
    #             countdown_ads_usa = get_text(engines,"/html/body/div[1]/div[3]/div/div[1]/div/div")
            
    #             if check_exists_by_xpath(engines,"/html/body/div[1]/div[3]/div/div[1]/div/div"):
    #                 if "Skip Ad" in get_text(engines,"/html/body/div[1]/div[3]/div/div[2]/button/div[1]"):
    #                     click(engines,"/html/body/div[1]/div[3]/div/div[2]/button/div[1]")
    #                     print("SKIP AD")
    #                     time.sleep(1)
    #                     break
    #             else:
    #                 break               
    #             # else:
    #             #     click(engines,"/div/div[3]/div/div[2]/button")
    #             #     print("skip ad /div/div[3]/div/div[2]/button")
    #     elif check_exists_by_xpath(engines,"/html/body/div/div/div[2]"):
    #         print("kemungkinan ads terakhir")
    #         print(get_text(engines,"/html/body/div/div/div[2]"))
    #         click(engines,"/html/body/div/div/div[2]")
    #     # else:
    #     #     break
    #         # time.sleep(1)
    #         # print("click skip")
    #         # engines.save_screenshot('ss/image.png')
    #         # while True:
    #         #     engines.save_screenshot('ss/image.png')
    #         #     if check_exists_by_xpath(engines,"/html/body/div[1]/div[3]/div/div[2]/button"):
    #         #         engines.find_element(By.XPATH,"/html/body/div[1]/div[3]/div/div[2]/button").click()
    #         #         break
    #         #     else:
    #         #         break
    # # else:
    # #     print("NO ADS")
    # #     engines.save_screenshot('ss/image.png')
    # #     break
    # # duration = get_text(engines,"/html/body/main/div/article/div[2]/div/div[1]/div/div[2]/div[9]/span")
    # # print(duration)
