import time
import sys
import subprocess
import os
import random
import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities    

myOs=sys.platform

if myOs.lower() == "linux":
    from pyvirtualdisplay import Display
    display = Display(visible=False, extra_args=[':{0}'.format(random.randint(1,9000))], size=(2560,1600), backend="xvfb")
    display.start()

def run_command(command):
    return subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()

def get_chrome_main_version():
    google_version = run_command('google-chrome --version').decode().strip().split(" ")[2].split(".")[0]
    return google_version

def getproxy(filename,number):
    number -= 1
    proxy_file = open("proxylist.txt", "r")
    akun_file = open(filename,"r")
    proxy_list = list(map(lambda s: s.strip(), proxy_file.readlines()))
    akun_list = akun_file.readlines()
    jumlah_proxy = len(proxy_list)
    jumlah_akun = len(akun_list)
    diff_9 = list(range(number, jumlah_akun, jumlah_proxy))
    # print(diff_9)
    while True:
        if diff_9[0] < jumlah_proxy:
            diff_9[0]  = diff_9[0]
            # print(diff_9,True,proxyselector)
            break
        else:
            diff_9[0] = diff_9[0] - jumlah_proxy
            # print(diff_9,False,proxyselector)
            pass
    
    proxy_choose = proxy_list[diff_9[0]] 
    return proxy_choose

def engine():
    start = time.time()
    number = 1
    position_x = int(number*20)
    position_y = int(number*50)
    # login = data[1].strip().split(":")
    # username = login[0]
    # password = login[1]
    headless = False
    filename = "./urllist.txt"
    # PROXY_HOST = getproxy(filename,number)
    # print(PROXY_HOST)
    
    chrome_options = uc.ChromeOptions()
    # chrome_options.add_argument('--proxy-server=socks5://'+PROXY_HOST)
    
    # chrome_options.add_argument("--window-size=720,968")
    chrome_options.add_argument("--window-position={0},{1}".format(position_x,position_y))
    # unpackerproxyext = "proxy"
    # cookies_directory = os.path.dirname(os.path.realpath(__file__)).replace("module","")
    # unpackerproxyext = f"{cookies_directory}{unpackerproxyext}"

    # chrome_options.add_argument("--load-extension=" + kecap + "," + kue)
    # chrome_options.add_argument("--load-extension=" + unpackerproxyext)
    chrome_options.add_argument("--load-extension=./kecap")

    # if myOs.lower() == "linux":
    #     # chrome_options.add_argument("--headless") # Ensure GUI is off
    #     chrome_options.add_argument("--window-size=720,968")
    if headless:
        chrome_options.add_argument("--headless") # Ensure GUI is off
    # chrome_options.add_argument("--headless") # Ensure GUI is off
    #chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument('--disable-popup-blocking')
    # user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
    # chrome_options.add_argument(f'user-agent={user_agent}')
    capabilities = DesiredCapabilities.CHROME.copy()
    capabilities['goog:loggingPrefs'] = {'performance': 'ALL'}
    if myOs.lower() == "linux":
        version_main = get_chrome_main_version()
        browser = uc.Chrome(options=chrome_options,use_subprocess=True,version_main=version_main,desired_capabilities=capabilities)
    else:
        browser = uc.Chrome(options=chrome_options,use_subprocess=True,version_main='110',desired_capabilities=capabilities)
    
    return number,browser
