from engine import engine
from ads_validation import ads_validation
import time
import sys
import random
import json
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException, ElementNotVisibleException, ElementNotSelectableException


myOs=sys.platform



def check_exists_by_xpath(browser,xpath):
    try:
        browser.find_element(By.XPATH,xpath)
        browser.implicitly_wait(10)
    except NoSuchElementException:
        return False
    return True

def retry_find_element(browser,xpath,number,worker):
    count = 0

    while True:
        count +=1
        print(f"{number}. {worker} : Trying {count}!! ")

        try:
            # Wait for up to 10 seconds for the element with the ID "my-element" to be located
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
        except TimeoutException:
            # If the element is not found, try again
            continue
        else:
            # If the element is found, break out of the loop
            return True
    # count = 0
    # while True:
    #     check_xpath = check_exists_by_xpath(driver,xpath)
    #     if count < 20000:
    #         count +=1
    #         if check_xpath == True:
    #             return True

def click(driver,xpath):
    driver.find_element(By.XPATH,xpath).click()

def filling(driver,xpath,value):
    driver.find_element(By.XPATH,xpath).send_keys(value)

def get_text(driver,xpath):
    return driver.find_element(By.XPATH,xpath).text

while True:
    engines = engine()[1]
    # print(engines)
    #links = ['https://rumble.com/v2bwuka-pivot-point.html','https://rumble.com/v293rba-cool-traditional-balinese-rindik-music-to-relax.html','https://rumble.com/v293n8m-amazing-gamelan-music-indonesia.html','https://rumble.com/v28iy7s-wkwkwk-face-lol.html','https://rumble.com/v28ixns-tfv-chanel-video-opening.html','https://rumble.com/v28ixjm-just-jokes.html','https://rumble.com/v28ixa5-is-there-an-opponent.html','https://rumble.com/v28ix4m-super-lol.html','https://rumble.com/v28iwy8-omg-funny-lol.html','https://rumble.com/v28iwr0--dance.html','https://rumble.com/v28iwne--dance.html','https://rumble.com/v28iwha-new-years-eve-cant-believe-only-hours-till-2023-i-love-you-guys-happy-new-y.html','https://rumble.com/v28iwd4-love-this-audio.html','https://rumble.com/v28iw7u-versi-gampang-rubber-band-niy.html','https://rumble.com/v28iw2a-the-dance-is-really-cool.html','https://rumble.com/v28fzt8-most-funniest-tiktok-videos-in-the-road.html','https://rumble.com/v28fzf2-did-you-succeed-i-couldnt-keep-it-inside..html','https://rumble.com/v28fy7g-hilarious-ignorant-statue...html','https://rumble.com/v28fzkm-funniest-holy-ghost-legion.html','https://rumble.com/v28fyr6-finding-new-brand-step-mom.html','https://rumble.com/v28fxdf-jog-tiktok-funny-makes-you-laugh.html','https://rumble.com/v28fyh6-crazy-shaman-tiktok.html','https://rumble.com/v28fy8k-hok-aa-hok-ee.html','https://rumble.com/v28fxp3-beautiful-funny-tiktok.html','https://rumble.com/v28fvmy-the-funniest-scariest-animal-encounters-damn-nature-you-scary.html','https://rumble.com/v28fvla-animals-contacted-the-wrong-opponent-30-times..html','https://rumble.com/v28fuos-animals-who-interacted-with-the-wrong-opponent-40-times.html','https://rumble.com/v28ftte-very-worse-of-your-fate-thief-got-hit-by-a-block-when-in-action-in-the-dayt.html','https://rumble.com/v28fspy-entertainment-for-earth-things-random-fools-entertainment-for-citizens-62-m.html','https://rumble.com/v28bl84-laughing-makes-you-joyful.html','https://rumble.com/v28b8us-humans-being-idiots-the-best-of-stupidity.html']
    # Using readlines()
    file1 = open('urllist.txt', 'r')
    links = file1.readlines()
    for link in links:
        print(link)
        # engines.get("https://ipinfo.io")
        # time.sleep(100000)
        engines.get(link)
        time.sleep(2)
        meta_json_video = json.loads(engines.execute_script('return document.querySelector("head > script:nth-child(21)").innerText'))[0]
        title = meta_json_video['name']
        description = meta_json_video['description']
        thumbnailUrl = meta_json_video['thumbnailUrl']
        uploadDate = meta_json_video['uploadDate']
        duration = meta_json_video['duration']
        duration = duration.replace('PT', '')
        duration_hour = duration.split("H")[0]
        duration_minute = duration.split("H")[1].split('M')[0]
        duration_second = duration.split("H")[1].split('M')[1].replace('S','')
        durations = f"{duration_hour} H, {duration_minute} M, {duration_second} S "
        duration_to_second = (int(duration_hour)*3600)+(int(duration_minute)*60)+int(duration_second)
        embedUrl = meta_json_video['embedUrl']
        url = meta_json_video['url']
#        views = meta_json_video['interactionStatistic']['userInteractionCount']
        width = meta_json_video['width']
        height = meta_json_video['height']
        # videoQuality = meta_json_video['videoQuality']
        
        username = get_text(engines,"/html/body/main/div/article/div[2]/div/div[3]/div[1]/a/div/div[1]")
        follower = get_text(engines,"/html/body/main/div/article/div[2]/div/div[3]/div[1]/a/div/div[2]")
        # uploaded_since = get_text(engines,"/html/body/main/div/article/div[2]/div/div[4]")
        # title_video = get_text(engines,"/html/body/main/div/article/div[2]/div/h1")
        thumbs_up = get_text(engines,"/html/body/main/div/article/div[2]/div/div[5]/div[1]/button[1]/span")
        thumbs_down = get_text(engines,"/html/body/main/div/article/div[2]/div/div[5]/div[1]/button[2]/span")
        views = get_text(engines,"/html/body/main/div/article/div[2]/div/div[5]/div[2]/div/div")
        print(f"Link: {link}\nUser : {username}\nFollower: {follower}\nvideo uploaded: {uploadDate}\nTitle: {title}\nThumbs up: {thumbs_up}\nThumbs down: {thumbs_down}\nViews: {views}\nDuration: {duration_to_second} detik")
        print("Watching...")
        # time.sleep(500000)
        engines.find_element(By.XPATH,"/html/body/main/div/article/div[2]/div/div[1]/div/div[1]/div").click()
        ads_validation(engines)
        time.sleep(duration_to_second if duration_to_second < 60 else 60)
        print("DONE. NEXT VIDEO!")
    engines.close()
    engines.quit()
        # then do stuff in the frame
        # engines.switch_to.default_content()
        # then do stuff on main window again

        # time.sleep(500000)
        # time.sleep(500000)
