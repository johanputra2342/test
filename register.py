from engine import engine
from ads_validation import ads_validation
import time
import sys
import random
import json
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException, ElementNotVisibleException, ElementNotSelectableException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

myOs=sys.platform

def getdatainbox():
    from mailbox import get_data_inbox

    inbox_content = get_data_inbox()
    # print(inbox_content)
    # email = "bayuandi@pytorch.institute"


    # if inbox_content[1] == email and inbox_content[2] == 'Account request confirmed':
    #     print("{0} is confirmed".format(email))
    # elif inbox_content[1] == email and inbox_content[2] == 'Verify your email':
    #     print("{0} is waiting for verify".format(email))
    return inbox_content


def check_exists_by_xpath(browser,xpath):
    try:
        browser.find_element(By.XPATH,xpath)
        browser.implicitly_wait(10)
    except NoSuchElementException:
        return False
    return True

def retry_find_element(browser,xpath):
    count = 0

    while True:
        count +=1
        print(f"Trying {count}!! ")

        try:
            # Wait for up to 10 seconds for the element with the ID "my-element" to be located
            wait = WebDriverWait(browser, 10)
            element = wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
        except TimeoutException:
            # If the element is not found, try again
            continue
        else:
            # If the element is found, break out of the loop
            return True
    # count = 0
    # while True:
    #     check_xpath = check_exists_by_xpath(driver,xpath)
    #     if count < 20000:
    #         count +=1
    #         if check_xpath == True:
    #             return True

def click(driver,xpath):
    driver.find_element(By.XPATH,xpath).click()

def filling(driver,xpath,value):
    driver.find_element(By.XPATH,xpath).send_keys(value)

def get_text(driver,xpath):
    return driver.find_element(By.XPATH,xpath).text
def month_hbd(month):
    # if month == "01":
    #     month = "January"
    # if month == "02":
    #     month = "February"
    # if month == "03":
    #     month = "March"
    # if month == "04":
    #     month = "April"
    # if month == "05":
    #     month = "May"
    # if month == "06":
    #     month = "June"
    # if month == "07":
    #     month = " July"
    # if month == "08":
    #     month = "August"
    # if month == "09":
    #     month = "September"
    # if month == "10":
    #     month = "October"
    # if month == "11":
    #     month = "November"
    # if month == "12":
    #     month = "December"
    return int(month)

def registration_data(engines,fake_name,username,password,email_generated,day,month,year):
    if "." in username:
        username = username.replace(".","")
    #name
    filling(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[1]/span/input",fake_name)
    #username
    filling(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[2]/span/input",random.choice(username))
    #password
    filling(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[3]/span/input",password)
    #confirm password
    filling(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[4]/span/input",password)
    #email
    filling(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[5]/span/input",email_generated)
    #confirm email 
    filling(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[6]/span/input",email_generated)
    #country click
    click(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[7]/span/select")
    time.sleep(1)
    #country USA
    click(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[7]/span/select/option[2]")
    time.sleep(1)
    click(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[6]/span/input")
    
    #ActionChains(engines).send_keys(Keys.ENTER).perform()
    # click(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/div[2]/ul/li[7]/span/select/option[2]")
    # 6 | click | xpath=//select[@id='birthday-day'] |  | 
    engines.find_element(By.XPATH, "//select[@id=\'birthday-day\']").click()
    # 7 | select | id=birthday-day | label=4 | 
    dropdown = engines.find_element(By.ID, "birthday-day")
    dropdown.find_element(By.XPATH, f"//option[. = {day}]").click()
    # 8 | click | id=birthday-month |  | 
    engines.find_element(By.ID, "birthday-month").click()
    # 9 | select | id=birthday-month | label=March | 
    # time.sleep(50000)
    dropdown = engines.find_element(By.ID, "birthday-month")
    dropdown.find_element(By.XPATH, f"//*[@id=\"birthday-month\"]/option[{month+1}]").click()
    # 10 | click | id=birthday-year |  | 
    engines.find_element(By.ID, "birthday-year").click()
    # 11 | select | id=birthday-year | label=2009 | 
    dropdown = engines.find_element(By.ID, "birthday-year")
    dropdown.find_element(By.XPATH, f"//option[. = {year}]").click()
    #agreement
    click(engines,"//form[@id='registration']/ul/li[2]/span/div/label")
    
    
    #submit
    click(engines,"/html/body/main/div/div/div[1]/section/div[2]/div/div/form/ul/li[3]/div[1]/input")
    # if retry_find_element(engines,"/html/body/main/div/div/div[2]/div[2]/p"):
    # from python_anticaptcha import AnticaptchaClient, NoCaptchaTaskProxylessTask

    # api_key = 'b1b42fe0b22b0430ce617f572bdfcfc9'
    # site_key = engines.find_element(By.XPATH, "/html/body/main/div/div/div[1]/section/div[2]/div/div/form/ul/li[4]/div").get_attribute("data-sitekey")  # grab from site
    # url = 'https://rumble.com/register.php'
    # print(site_key)
    # client = AnticaptchaClient(api_key)
    # task = NoCaptchaTaskProxylessTask(url, site_key, is_invisible=True)
    # job = client.createTask(task)
    # job.join()
    # print(job.get_solution_response())

    # engines.execute_script("""document.querySelector('[name="g-recaptcha-response"]').innerText='{}'""".format(job.get_solution_response()))
    
    
    
    return True

while True:
    engines = engine()[1]

    link = "https://rumble.com/register.php"
    engines.get(link)


    from faker import Faker
    fake = Faker('en_US')
    
    while True:
        birthday = fake.date().split("-")
        if int(birthday[0]) > 1990:
            pass
        else:
            year = int(birthday[0])
            month = month_hbd(birthday[1])
            day = int(birthday[2])
            break
    
    # print(year,month,day)
    # time.sleep(500000)
    fake_name = fake.name()
    username = [str(random.randint(100,2000)).lower()+fake_name.replace(' ', '').lower(),fake_name.replace(' ', '').lower()+str(random.randint(100,2000)).lower(),fake_name.replace(' ', str(random.randint(100,2000))).lower()] 
    email_generated = random.choice(username)+ "@koplakmantap.com " 
    password = 'Jayakarta123@'
    

    if registration_data(engines,fake_name,username,password,email_generated,day,month,year):
        time.sleep(30)
        link = getdatainbox()
        time.sleep(10)
        # print(link)
        engines.get(link)
        if check_exists_by_xpath(engines,"/html/body/main/div/section/div[1]/p[1]"):
            if "If you have a" in get_text(engines,"/html/body/main/div/section/div[1]/p[1]"):
                print("Failed create account")
        else:
            print(f"{email_generated}:{password}")
            f = open("akun.txt", "a")
            f.write(f"{email_generated.strip()}:{password}\n")
            f.close()
            time.sleep(5)
    
    engines.close()
    engines.quit()

