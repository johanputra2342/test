FROM python:3.8.5
COPY . /app
WORKDIR /app
RUN apt -y update
RUN apt install -y sudo unzip screen npm wget build-essential gcc nano
RUN sudo apt install -y xvfb
RUN sudo npm install -g pm2
#RUN mkdir plugin && cp plugin.zip plugin && cd plugin && unzip plugin.zip
RUN pip install --upgrade pip
RUN pip install setuptools-rust
RUN pip install -r requirements.txt
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN sudo apt-get install -y ./google-chrome-stable_current_amd64.deb
RUN sudo apt-get install -y -f
RUN rm google-chrome-stable_current_amd64.deb
# RUN wget https://chromedriver.storage.googleapis.com/106.0.5249.61/chromedriver_linux64.zip
# RUN unzip chromedriver_linux64.zip
# Check chrome version
RUN echo "Chrome: " && google-chrome --version
#RUN cd stratum && npm install && cd ..
#ENV ACCESS_TOKEN="yM40VtnWYbLrVvsdPQR1QUAZOLFgGqPbOoKzOmda"
# RUN sudo apt-get install ./google-chrome-stable_current_amd64.deb
ENTRYPOINT ["python"]
#CMD ["main.py"]

EXPOSE 8888

CMD ["rumble.py"]
